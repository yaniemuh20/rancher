# Prometheus

An Ansible role that installs Prometheus standalone to  CENTOS 7. More info about Protmetheus: [link](https://prometheus.io)

## Requirements

Make sure, you made your vars file with valid path to custom template or you can use Default vars.

You need ansible 2.9.9 to run this module

## Role Variables

Available variables are listed below, along with default values.
- yum_packages - default useful pkgs install
- prometheus_system_group - prometheus system group
- aide_cron_hour - hour, that the job should run (0 - 23)

## Tasks descriptions

- main.yml - Run the OS check and run the relevant playbook
- dependencies.yml - Install dependencies and create users / groups
- install.yml - Install promtheus on Centos 7.x
- configuration.yml - Run SElinux and firewall config
- housekeeping.yml - run housekeeping

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servergroup
      become: true
      become_user: lee
    
      vars_files:
        - vars/main.yml
        
      roles:
        - nerve4-prometheus


## Author Information
Lee P. | 2020


## License

This project is licensed under the MIT License